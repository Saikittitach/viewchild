import { ChildComponentComponent } from './child-component/child-component.component';
import { Component, ViewChild} from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  currentItem = 'pass-data';
  Counter = 5;


  @ViewChild(ChildComponentComponent,{static : true}) test: ChildComponentComponent;

  constructor(){

  }

  countChangedHandler(count: number){
    this.Counter = count;
    console.log(count);
  }

  increment(){
    // this.child.increment();
  }

}
