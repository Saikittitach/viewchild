import { ThisReceiver } from '@angular/compiler';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';


@Component({
  selector: 'app-child-component',
  templateUrl: './child-component.component.html',
  styleUrls: ['./child-component.component.css']
})
export class ChildComponentComponent implements OnInit {

  @Input() count: number = 0;


  // @Output() countChanged: EventEmitter<number> = new EventEmitter();


  constructor(){

  }

  ngOnInit(): void {
  }

  // increment(){
  //   this.count++;
  //   this.countChanged.emit(this.count);
  // }

  increment(){
    this.count++;
  }
}
